package com.google.zxing.client.android.result;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.zxing.client.android.PreferencesActivity;
import org.apps4fi.HelMet.R;
import com.google.zxing.client.result.ISBNParsedResult;
import com.google.zxing.client.result.ParsedResult;

public class HelmetResultHandler extends ResultHandler {

	  private static final int[] buttons = {
	      R.string.button_helmet_search,
	      R.string.button_book_search,
	      R.string.button_search_book_contents
	  };

	  private String customProductSearch;

	  public HelmetResultHandler(Activity activity, ParsedResult result) {
	    super(activity, result);
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
	    customProductSearch = prefs.getString(PreferencesActivity.KEY_CUSTOM_PRODUCT_SEARCH, null);
	    if (customProductSearch != null && customProductSearch.length() == 0) {
	      customProductSearch = null;
	    }
	    Log.d("botsbot", "HelmetResultHandler...");
	  }

	  @Override
	  public int getButtonCount() {
	    // Always show four buttons - Shopper and Custom Search are mutually exclusive.
	    return buttons.length;
	  }

	  @Override
	  public int getButtonText(int index) {
	    if (index == buttons.length - 1 && customProductSearch != null) {
	      return R.string.button_custom_product_search;
	    }
	    return buttons[index];
	  }

	  @Override
	  public void handleButtonPress(final int index) {
	    showNotOurResults(index, new AlertDialog.OnClickListener() {
	      public void onClick(DialogInterface dialogInterface, int i) {
	        ISBNParsedResult isbnResult = (ISBNParsedResult) getResult();
	        switch (index) {
	          case 0:
	            openProductSearch(isbnResult.getISBN());
	            break;
	          case 1:
	            openBookSearch(isbnResult.getISBN());
	            break;
	          case 2:
	            searchBookContents(isbnResult.getISBN());
	            break;
	          case 3:
	            if (customProductSearch != null) {
	              String url = customProductSearch.replace("%s", isbnResult.getISBN());
	              openURL(url);
	            } else {
	              openGoogleShopper(isbnResult.getISBN());
	            }
	            break;
	        }
	      }
	    });
	  }

	  @Override
	  public int getDisplayTitle() {
	    return R.string.result_isbn;
	  }
	  @Override
	  protected void showNotOurResults(int index, AlertDialog.OnClickListener proceedListener) {
		  //Let's skip this dialog  
		  proceedListener.onClick(null, index);
	  }
	}
