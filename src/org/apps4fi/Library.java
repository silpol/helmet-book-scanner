package org.apps4fi;

public class Library {

	private String name;
	private String fullname;
	private String address;
	private String description;
	private String source;
	private double lat;
	private double lon;
	private float distance;
	
	public Library(String name, String fullname, String address, double lat, double lon){
		this.name = name;
		this.fullname = fullname;
		this.address = address;
		this.lat = lat;
		this.lon = lon;
	}
	
	public void setSource(String source){
		this.source = source;
	}

	public String getName(){
		return this.name;
	}
	
	public String getAddress(){
		return this.address;
	}
	
	public double getLatitude(){
		return this.lat;
	}

	public double getLongitude(){
		return this.lon;
	}
	
	public boolean containsLocation(){
		if (lat != 0.0 && lon != 0.0) return true;
		else return false;
	}
	
	public double distance(double latitude, double longitude){			
		if (latitude == this.lat && longitude == this.lon) return 0.0;
		
		double dist = Math.acos(Math.cos(this.lat * (Math.PI/180)) *
		     Math.cos(this.lon * (Math.PI/180)) *
		     Math.cos(latitude * (Math.PI/180)) *
		     Math.cos(longitude * (Math.PI/180)) +
		     Math.cos(this.lat * (Math.PI/180)) *
		     Math.sin(this.lon * (Math.PI/180)) *
		     Math.cos(latitude * (Math.PI/180)) *
		     Math.sin(longitude * (Math.PI/180)) +
		     Math.sin(this.lat * (Math.PI/180)) *
		     Math.sin(latitude * (Math.PI/180))) * 6371;   
		
		return dist;
	}
	public void setDistance(float dist){
		this.distance = dist;
	}

	public float getDistance(){
		return this.distance;
	}
	
}
