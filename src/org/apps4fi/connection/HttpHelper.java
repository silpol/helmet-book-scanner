package org.apps4fi.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apps4fi.Library;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.InputSource;


import android.location.Location;
import android.util.Log;

public class HttpHelper {
	// Debugging
    private static final String TAG = "botsbot";
	private String urlToSendRequest;
	private String targetDomain;
	private String xmlContentToSend;
	private String password;
	private String username;
	private Boolean autopublish = false;

	DefaultHttpClient httpClient;
	       
	HttpHost targetHost;
	// Using POST here
	HttpPost httpPost;
	private String isbn;
	private double lat_user;
	private double lon_user;
	public static ArrayList<Library> libraries;
	
	public HttpHelper(String isbn){
		this.isbn = isbn;
		urlToSendRequest =  "http://www.botsbot.com/helmet/search.php?q=" + isbn+ "&output=json";
		Log.d(TAG, "sending request: " + urlToSendRequest);
		targetDomain = "www.botsbot.com";

		httpClient = new DefaultHttpClient();
	       
		targetHost = new HttpHost(targetDomain, 80, "http");
		// Using POST here
		httpPost = new HttpPost(urlToSendRequest);
		
	}
	
	public Hashtable<String,String> sendRequest(){
		//String for ErrorView
		String error = "";
        Hashtable ht = new Hashtable<String,String>();
        String result = "";
		try
		{
			Log.d(TAG, "sending request...");
			// execute is a blocking call, it's best to call this code in a thread separate from the ui's
			HttpResponse response = httpClient.execute(targetHost, httpPost);
			Log.d(TAG, "sent!");
			error += "HTTP sent\n";
			if (response == null){
				error += "response == null\n";
				Log.d(TAG, "response == null");
			}
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity == null){
				error += "responseEntity == null\n";
				Log.d(TAG, "responseEntity == null");
			}
			//InputSource insrc = new InputSource(responseEntity.getContent());
			// A Simple JSON Response Read
            InputStream instream = responseEntity.getContent();
            result= convertStreamToString(instream);
            Log.i("botsbot",result);
		} catch (Exception ex){
			Log.d(TAG, "HttpHandler exception: " + ex.getMessage());
			Log.d(TAG, "HttpHandler trace: " + ex.toString());
			error += "HttpHandler exception: \n" + ex.getMessage() + "\n" + ex.toString() + "\n";
   			ht.put("error", error);
			return ht;
		}
		try
		{
			// A Simple JSONObject Creation
            JSONObject json=new JSONObject(result);
            String title = json.getString("title");
            String author = json.getString("author");
            String helmetref = json.getString("helmetref");
            String helmeturi = json.getString("helmeturi");
            String source = json.getString("source");

   			ht.put("title", title);
			ht.put("author", author);
			ht.put("helmetref", helmetref);
			ht.put("helmeturi", helmeturi);
			ht.put("source", source);
            
			libraries = new ArrayList<Library>();
			
            JSONObject kirjastot = json.getJSONObject("kirjastot");
            JSONArray nameArray=kirjastot.names();
            JSONArray valArray=kirjastot.toJSONArray(nameArray);            
			
            String s ="";
    		for (int i = 0; i < valArray.length(); i++) {
    			//String nimi = kirjastoArray.getJSONObject(i).getString("kokonimi").toString();
    			//String osoite = kirjastoArray.getJSONObject(i).getString("osoite").toString();
    			String name = nameArray.getString(i);
    			JSONObject kirjasto = valArray.getJSONObject(i);
    			String address = kirjasto.getString("osoite");
    			String lat_str = kirjasto.getString("lat");
    			String lon_str = kirjasto.getString("lon");
    			double lat = Double.valueOf(lat_str).doubleValue();
    			double lon = Double.valueOf(lon_str).doubleValue();

    			String fullname = kirjasto.getString("kokonimi");
    			s += name + "," + address + ", " + lat + ", " + lon + "\n";
    			Library l = new Library(name, fullname, address, lat, lon);
			
   		  		float[] results = new float[1];
   		  		Location.distanceBetween(lat_user, lon_user, lat, lon, results);
   		  		l.setDistance(results[0]); 
    			
    			libraries.add(l);
    		}
    		
            Log.v("HttpHandler", "Set response to responseEntity");
            
            Log.v("HttpHandler", "No error in parse");
            Log.d(TAG, "Otsikko: " + title + "\n" + "Kirjoittaja: " + author + "\n" + s);
            return ht;
			}catch (Exception ex){
    			Log.d(TAG, "HttpHandler exception: " + ex.getMessage());
    			Log.d(TAG, "HttpHandler trace: " + ex.toString());
    			error += "HttpHandler exception: \n" + ex.getMessage() + "\n" + ex.toString() + "\n";
    			////ht.put("error", error);
    			return ht;
    		}
	}
	private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

	public void setIsbn(String isbn){
		this.isbn = isbn;
	}

	public void setLat(double lat){
		this.lat_user = lat;
	}
	public void setLon(double lon){
		this.lon_user = lon;
	}
	
}
