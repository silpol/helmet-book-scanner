package org.apps4fi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import org.apps4fi.connection.HttpHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import org.apps4fi.connection.HttpHelper;
import org.apps4fi.HelMet.R;


public class HelMetActivity extends Activity{
	private static String isbn = "";
	// Need handler for callbacks to the UI thread
    final Handler mHandler = new Handler();
    private static Hashtable<String,String> mResults;
    
    ProgressDialog dialog;
    
    private LocationManager locationManager;
    private static Location location;
    private static ArrayList<Library> libraries = new ArrayList<Library>();
    
    // Create runnable for posting
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            updateUi();
        }
    };
	private org.apps4fi.HelMetActivity.EfficientAdapter ea;
	private Uri uri;

	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	        
	        setContentView(R.layout.helmet);
	        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.window_title);
	        TextView title = (TextView) findViewById(R.id.title);
	        title.setText(R.string.app_name);
	        ListView lv = (ListView)findViewById(R.id.libraries_listview); 
	        ea= new EfficientAdapter(this);
	        lv.setAdapter(ea);
	        updateLocation();
	        
	        if (savedInstanceState == null) {
	        	
	        	libraries = new ArrayList<Library>();
	        	Bundle extras = getIntent().getExtras();
		        if(extras !=null)
		        {
		        	isbn = extras.getString("isbn");
		        	dialog = ProgressDialog.show(this, "", getText(R.string.searching_helmet) +"\nISBN: " + isbn, true);
		        	startNetworkQuery();
			    } 
	        } 
	        
	        OnItemClickListener oicl = new OnItemClickListener() {
	        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        		if (libraries.size() == 0) return;
	        		Intent myIntent = new Intent(view.getContext(), LocationActivity.class);
	        		Library lb = ((Library) libraries.get(position));
	        		myIntent.putExtra("lat", lb.getLatitude());
	        		myIntent.putExtra("lon", lb.getLongitude());
	        		myIntent.putExtra("address", lb.getAddress());
	        		myIntent.putExtra("name", lb.getName());
	        		myIntent.putExtra("my_lat", location.getLatitude());
	        		myIntent.putExtra("my_lon", location.getLongitude());
	                startActivityForResult(myIntent, 0);
	        	}
	        };
	        lv.setOnItemClickListener(oicl);
	        
	        Button helmet = (Button) findViewById(R.id.button_go);
	        helmet.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View view) {
	            	launchIntent(new Intent(Intent.ACTION_VIEW, uri));
	            }
	        });

	 }
	
	private void updateLocation(){
	    //Get the location manager
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	
		// List all providers:
		List<String> providers = locationManager.getAllProviders();
		
		Criteria criteria = new Criteria();
		//TODO: May not be the best way to get location info
		String bestProvider = locationManager.getBestProvider(criteria, false);
		location = locationManager.getLastKnownLocation(bestProvider);
		
		Log.d("HelMet", "bestProvider: " + bestProvider + ",lat:" + location.getLatitude() + ",lon:" + location.getLongitude());
	}

	 @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.helmet_menu, menu);  
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
	        case R.id.menu_go_helmet:
	        	launchIntent(new Intent(Intent.ACTION_VIEW, uri));
	        	break;
	        case R.id.menu_share:
	        	share();
	        	break;
        }
        return true;
    }
	public void share() {
		String error = mResults.get("error");
		if (error != null && !error.equals("")){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setTitle("Virhe");
		        builder.setMessage("Tietoja ei voi jakaa!");
		        builder.setPositiveButton(R.string.button_ok, null);
		        builder.show();
		        return;
		}
		String title = mResults.get("title");
		String author= mResults.get("author");
		String helmeturi= mResults.get("helmeturi");
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);   	   
		shareIntent.setType("text/plain");
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getText(R.string.helmet_share_title));
		String entry = getText(R.string.helmet_author) + " " + author + "\n";
		entry += getText(R.string.helmet_title) + " " + title + "\n";
		entry += getText(R.string.helmet_link) + " " + helmeturi + "\n";

		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, entry);
		
		startActivity(Intent.createChooser(shareIntent, getText(R.string.menu_share_intent)));      
	}

	 protected void startNetworkQuery() {

	        // Fire off a thread to do some work that we shouldn't do directly in the UI thread
	        Thread t = new Thread() {

				public void run() {
	            	HttpHelper hh = new HttpHelper(isbn);
	            	hh.setLat(location.getLatitude());
	            	hh.setLon(location.getLongitude());
	            	mResults = hh.sendRequest();
	            	libraries = hh.libraries;
	            	if (libraries != null) {
		            	Collections.sort(libraries, new Comparator<Library>(){
		               	 
		                    public int compare(Library l1, Library l2) {
		                       return Float.compare(l1.getDistance(), l2.getDistance());
		                    }
		         
		                });
	            	}
	            	
	            	dialog.cancel();
	            	mHandler.post(mUpdateResults);
				}
	        };
	        t.start();
	    }
	 private void updateUi() {
		 if (mResults == null) return;
		 String error = mResults.get("error");
		 if (error != null && !error.equals("")){
			 AlertDialog.Builder builder = new AlertDialog.Builder(this);
		        builder.setTitle("Tapahtui mystinen virhe");
		        builder.setMessage(error);
		        builder.setPositiveButton(R.string.button_ok, null);
		        builder.show();
		        return;
		 }
		 
		 //No data for this 
		 String helmeturi= mResults.get("helmeturi");
		 if (helmeturi == null || helmeturi.equals("")){
			 AlertDialog.Builder builder = new AlertDialog.Builder(this);
		        builder.setTitle(getText(R.string.search_helmet_no_result_title));
		        builder.setMessage(getText(R.string.search_helmet_no_result_text));
		        builder.setPositiveButton(R.string.button_ok, null);
		        builder.show();
		        return;
		 }
		 String title = mResults.get("title");
		 String author= mResults.get("author");
		 uri = Uri.parse(helmeturi);
 
		 TextView nimike = (TextView)findViewById(R.id.helmet_nimike_text);
		 nimike.setText(title);
		 
		 TextView tekija = (TextView)findViewById(R.id.helmet_tekija_text);
		 tekija.setText(author);
		 Log.d("HelMet", "title: " + title + ", author: " + author);
		 
		 if (libraries != null){
			 ea.notifyDataSetChanged();
		 }
	 }
	 
	 void launchIntent(Intent intent) {
	    if (intent != null) {
	      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
	      try {
	        this.startActivity(intent);
	      } catch (ActivityNotFoundException e) {
	        AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle(R.string.app_name);
	        builder.setMessage(R.string.msg_intent_failed);
	        builder.setPositiveButton(R.string.button_ok, null);
	        builder.show();
	      }
	    }
	  }
	 
	@Override
    public void onRestoreInstanceState(Bundle savedInstanceState){
    	//This MUST be included otherwise when the phone screen is flipped some variables (mBluetoothAdapter) go null
    }
	
	public static class EfficientAdapter extends BaseAdapter {
    	private LayoutInflater mInflater;
    	private Context mContext;
    	
    	public EfficientAdapter(Context context) {
    		mInflater = LayoutInflater.from(context);
    		mContext = context;
    	}
    	public int getCount() {
    		return libraries.size();
    	}
    	
    	public Object getItem(int position) {
    		return position;
    	}
    	
    	public long getItemId(int position) {
    		return position;
    	}
    	
    	public View getView(int position, View convertView, ViewGroup parent) {
    		ViewHolder holder;
    		if (convertView == null) {
    			convertView = mInflater.inflate(R.layout.library_item, null);
    			holder = new ViewHolder();
    			holder.library = (TextView) convertView.findViewById(R.id.library_name);
    			holder.address = (TextView) convertView.findViewById(R.id.library_entry);
    			convertView.setTag(holder);
    	} else {
    		holder = (ViewHolder) convertView.getTag();
    	}
    		Library lb = libraries.get(position);
    		holder.library.setText(lb.getName());
			Float f = (Float) lb.getDistance()/1000;
			String s = "";
			if (f<0) {
				s = f.toString().substring(0, 3);
			} else if (f<10){
				s=f.toString().substring(0, 3); 
			} else{
				s=(new Long((Math.round(f.doubleValue())))).toString();
			}
			holder.address.setText(lb.getAddress() + ". " + mContext.getText(R.string.helmet_distance) + " " + s + " km"); 
    		return convertView;
    	}
    	
    	static class ViewHolder {
    		TextView library;
    		TextView address;
    	}
    }
	
}
